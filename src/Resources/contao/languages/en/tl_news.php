<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (C) 2005-2013 Leo Feyer
 *
 * @package   contao-news-simple-bundle
 * @author    Felix Pfeiffer : Neue Medien
 * @license   LGPL-3.0-or-later
 * @copyright 2013 Felix Pfeiffer : Neue Medien
 */

$GLOBALS['TL_LANG']['tl_news']['newsText'][0] = 'Newstext';
$GLOBALS['TL_LANG']['tl_news']['newsText'][1] = 'The newstext is shown in the news reader. You do not need content elements to add just a simple text.';

$GLOBALS['TL_LANG']['tl_news']['newstext_legend'] = 'Newstext';
