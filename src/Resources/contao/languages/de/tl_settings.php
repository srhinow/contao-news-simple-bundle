<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (C) 2005-2013 Leo Feyer
 *
 * @package   contao-news-simple-bundle
 * @author    Felix Pfeiffer : Neue Medien
 * @license   LGPL-3.0-or-later
 * @copyright 2013 Felix Pfeiffer : Neue Medien
 */

$GLOBALS['TL_LANG']['tl_settings']['newsSimpleNoElements'][0] = 'Keine Inhaltselemente in News';
$GLOBALS['TL_LANG']['tl_settings']['newsSimpleNoElements'][1] = 'Verhindert die Möglichkeit, Inhaltselemente in News einzusetzen.';

$GLOBALS['TL_LANG']['tl_settings']['simpleNews_legend'] = 'Simple-News';
