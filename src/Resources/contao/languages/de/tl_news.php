<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (C) 2005-2013 Leo Feyer
 *
 * @package   contao-news-simple-bundle
 * @author    Felix Pfeiffer : Neue Medien
 * @license   LGPL-3.0-or-later
 * @copyright 2013 Felix Pfeiffer : Neue Medien
 */

$GLOBALS['TL_LANG']['tl_news']['newsText'][0] = 'Nachrichtentext';
$GLOBALS['TL_LANG']['tl_news']['newsText'][1] = 'Der Nachrichtentext wird in einem Nachrichtenleser direkt angezeigt. Es muss nicht über Inhaltselemente extra Text eingegeben werden.';

$GLOBALS['TL_LANG']['tl_news']['newstext_legend'] = 'Nachrichtentext';
