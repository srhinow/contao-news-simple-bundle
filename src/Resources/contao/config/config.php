<?php

/**
 * @copyright 2018 Felix Pfeiffer : Neue Medien
 * @author    Felix Pfeiffer : Neue Medien
 * @author    Sven Rhinow
 * @package   contao-news-simple-bundle
 * @license   LGPL-3.0-or-later
 *
 */

/**
 * HOOKS
 *
 * Hooks are stored in a global array called "TL_HOOKS". You can register your
 * own functions by adding them to the array.
 *
 * $GLOBALS['TL_HOOKS'] = array
 * (
 *    'hook_1' => array
 *    (
 *       array('MyClass', 'myPostLogin'),
 *       array('MyClass', 'myPostLogout')
 *    )
 * );
 *
 * Hooks allow you to add functionality to the core without having to modify the
 * source code by registering callback functions to be executed on a particular
 * event. For more information see https://contao.org/manual.html.
 */
$GLOBALS['TL_HOOKS']['parseArticles']['newsSimple'] = array('Srhinow\ContaoNewsSimpleBundle\NewsSimple','addSimpleNewsText');