<?php

/**
 * @copyright 2018 Felix Pfeiffer : Neue Medien
 * @author    Felix Pfeiffer : Neue Medien
 * @author    Sven Rhinow
 * @package   contao-news-simple-bundle
 * @license   LGPL-3.0-or-later
 *
 */

namespace Srhinow\ContaoNewsSimpleBundle\ContaoManager;

use Contao\CoreBundle\ContaoCoreBundle;
use Contao\ManagerPlugin\Bundle\Config\BundleConfig;
use Contao\ManagerPlugin\Bundle\BundlePluginInterface;
use Contao\ManagerPlugin\Bundle\Parser\ParserInterface;

use Srhinow\ContaoNewsSimpleBundle\SrhinowContaoNewsSimpleBundle;

/**
 * Plugin for the Contao Manager.
 *
 * @author Sven Rhinow
 */
class Plugin implements BundlePluginInterface
{
    /**
     * {@inheritdoc}
     */
    public function getBundles(ParserInterface $parser)
    {
        return [
            BundleConfig::create(SrhinowContaoNewsSimpleBundle::class)
                ->setLoadAfter([ContaoCoreBundle::class])
        ];
    }
}
