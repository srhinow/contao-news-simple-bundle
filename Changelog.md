# Changes in srhinow/contao-news-simple-bundle
# 0.1.0 (02.03.2023)
- Fix: license to LGPL-3.0-or-late
- fix and rewrite with Contao 4.13 and PHP 8.2
- update require in composer.json

# 0.0.2 (25.12.2018)
- Fix: addImageToTemplate, Fix @package string, add "use"s
- new comments
- fix Bundle-Name and @package name
- fix @package name
- fix @package name and remove unused comment

# 25.12.2018
- initital commit